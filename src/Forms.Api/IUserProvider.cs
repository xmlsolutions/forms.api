﻿using System;
using System.Threading.Tasks;

namespace Forms.Api
{
	public interface IUserProvider
	{
        /// <summary>
        ///     Return details of the logged in user 
        /// </summary>
        /// <param name="systemUserIdentifier">
        ///     Identifier of the currently logged in user
        /// </param>
        /// <param name="tenantKey">
        ///     Identifier of the tenant, null unless supplied during form creation
        /// </param>
        /// <returns>User</returns>
        Task<User> GetUserDetail(SystemUserIdentifier systemUserIdentifier, string tenantKey);
    }
}
