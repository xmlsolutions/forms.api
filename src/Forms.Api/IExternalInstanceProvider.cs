﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forms.Api
{
    public interface IExternalInstanceProvider
    {
        Task<IReadOnlyCollection<BuildingBlockInstance>> GetInstance(Guid instanceGuid, string tenantKey);
    }
}
