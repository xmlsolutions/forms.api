﻿using System;
using System.Threading.Tasks;
using FormAbstractions;

namespace Forms.Api
{
	public interface IAuthorisationProvider
	{
        /// <summary>
        ///     Determine if the current logged in user is authorised to carry out a particular action
        /// </summary>
        /// <param name="instanceGuid">
        ///     A GUID identifier that represents the instance of a form for a particular subject
        ///     Can be null on a create
        /// </param>
        /// <param name="systemUserIdentifier">
        ///     Identifier of the current user requiring authentication
        /// </param>
        /// <param name="subjectIdentifier">
        ///     Identifier of the subject
        /// </param>
        /// <param name="formKey">
        ///     Form key
        /// </param>
        /// <param name="formVersion">
        ///     Form Version
        /// </param>
        /// <param name="formAction">
        ///     Action being taken
        /// </param>
        /// <param name="instanceCreator">
        ///  Identifier of the user who created the form instance
        /// </param>
        /// <param name="tenantKey">
        ///     Identifier of the tenant
        /// </param>
        /// <param name="onBehalfOf">
        ///     Optional identifier for when a user is acting on behalf of another user
        /// </param>
        /// <returns>Returning true or false as to whether they are authorised</returns>
        Task<bool> UserHasAuthority(
		    Guid? instanceGuid,
            SystemUserIdentifier systemUserIdentifier,
            string subjectIdentifier,
            string formKey,
            int formVersion,
		    FormAction formAction, 
            UserIdentifier instanceCreator,
		    string tenantKey,
            UserIdentifier? onBehalfOf);


        /// <summary>
        ///  Determine if the provided user can carry out a particular system action.  (A system action may span multiple subjects)
        /// </summary>
        /// <param name="systemUserIdentifier">
        ///      Identifier of the current user requiring authentication
        /// </param>
        /// <param name="action">
        ///      Action being taken
        /// </param>
        /// <param name="tenantKey">Identifier of the tenant</param>
        /// <returns></returns>
        Task<bool> UserHasAuthority(SystemUserIdentifier systemUserIdentifier, SystemAction action, string tenantKey);
    }
}
