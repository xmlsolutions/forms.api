﻿using System;
using System.Collections.Generic;

namespace Forms.Api
{
    public sealed record ReferenceDataItem
    {
        public string Label { get; }
        public string Value { get; }
        public string[] FilterGroups { get; }
        public bool IsStopCondition { get; }
        public string? IsStopConditionText { get; }
        public bool IsDeprecated { get; }
        public bool IsExclusive { get; }

        public ReferenceDataItem(
            string label,
            string value,
            string[] filterGroups,
            bool isStopCondition = false,
            string? isStopConditionText = null,
            bool isDeprecated = false,
            bool isExclusive = false)
        {
            ArgumentNullException.ThrowIfNull(filterGroups);
            ArgumentException.ThrowIfNullOrEmpty(label);
            ArgumentException.ThrowIfNullOrEmpty(value);

            Label = label;
            Value = value;
            FilterGroups = filterGroups;
            IsStopCondition = isStopCondition;
            IsStopConditionText = isStopConditionText;
            IsDeprecated = isDeprecated;
            IsExclusive = isExclusive;
        }
    }
}
