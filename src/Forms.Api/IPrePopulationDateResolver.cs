﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Forms.Api
{
    public interface IPrePopulationDateResolver
    {
        Task<DateTime?> Resolve(string subjectIdentifier, string buildingBlockKey, int buildingBlockVersion, string prePopulationRule, string tenantKey);
    }
}
