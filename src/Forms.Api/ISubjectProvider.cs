﻿using System;
using System.Threading.Tasks;

namespace Forms.Api
{
	public interface ISubjectProvider
	{
        /// <summary>
        ///     Get details of a particular subject
        /// </summary>
        /// <param name="instanceGuid">
        ///     A GUID identifier that represents the instance of a form for a particular subject
        /// </param>
        /// <param name="subjectIdentifier">
        ///     Identifier of the subject
        /// </param>
        /// <param name="tenantKey">
        ///     Identifier of the tenantKey, null unless supplied during form creation
        /// </param>
        /// <returns>Subject</returns>
        Task<Subject> GetSubjectDetail(Guid instanceGuid, string subjectIdentifier, string tenantKey);
	}
}
