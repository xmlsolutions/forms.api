﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Forms.Api.Notifications
{
    public sealed record ExternalNotification
    {
        public Guid InstanceGuid { get; }
        public Guid InstanceVersionGuid { get;}
        public string SubjectIdentifier { get; }
        public FormAbstractions.UserIdentifier UserIdentifier { get; }
        public string TenantKey { get; }
        public string FormKey { get; }
        public int FormVersion { get; }
        public string Action { get; }
        public string CorrelationId { get; }
        public IReadOnlyCollection<KeyValuePair<string, XElement>> ValuesForExport { get; }
        public string FormTitle { get; }
        public bool? PreviouslySubmitted { get; }

        public ExternalNotification(
            string subjectIdentifier,
            FormAbstractions.UserIdentifier userIdentifier,
            string tenantKey,
            string correlationId,
            Guid instanceGuid,
            Guid instanceVersionGuid,
            string formKey,
            int formVersion,
            string action,
            IReadOnlyCollection<KeyValuePair<string, XElement>> valuesForExport,
            string formTitle,
            bool? previouslySubmitted)
        {
            ArgumentNullException.ThrowIfNull(userIdentifier);
            ArgumentNullException.ThrowIfNull(valuesForExport);

            ArgumentException.ThrowIfNullOrEmpty(tenantKey);
            ArgumentException.ThrowIfNullOrEmpty(subjectIdentifier);
            ArgumentException.ThrowIfNullOrEmpty(action);
            ArgumentException.ThrowIfNullOrEmpty(formKey);
            ArgumentException.ThrowIfNullOrEmpty(correlationId);

            SubjectIdentifier = subjectIdentifier;
            UserIdentifier = userIdentifier;
            TenantKey = tenantKey;
            Action = action;
            ValuesForExport = valuesForExport;
            CorrelationId = correlationId;
            InstanceGuid = instanceGuid;
            InstanceVersionGuid = instanceVersionGuid;
            FormKey = formKey;
            FormVersion = formVersion;
            FormTitle = formTitle;
            PreviouslySubmitted = previouslySubmitted;
        }
    }
}
