﻿using System.Threading.Tasks;

namespace Forms.Api.Notifications
{
    public interface IExternalNotificationHandler
    {
        Task Invoke(ExternalNotification request);
    }
}
