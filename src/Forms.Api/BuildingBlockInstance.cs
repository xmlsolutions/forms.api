﻿using System;
using System.Xml.Linq;

namespace Forms.Api
{
    public sealed record BuildingBlockInstance
    {
        public string Key { get; }
        public int Version { get; }
        public XElement Data { get; }

        public BuildingBlockInstance(
            string key,
            int version,
            XElement data)
        {
            ArgumentNullException.ThrowIfNull(data);
            ArgumentException.ThrowIfNullOrEmpty(key);

            Key = key;
            Version = version;
            Data = data ?? throw new ArgumentNullException(nameof(data));
        }
    }
}
