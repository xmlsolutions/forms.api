﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forms.Api
{
    public interface IServerSideCalculationResolver
    {
        Task<string> Calculate(string key, int version, Dictionary<string, string> valuesOfInterest, string tenantKey);
    }
}