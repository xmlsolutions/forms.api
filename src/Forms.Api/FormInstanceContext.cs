﻿using System;
using System.Collections.Generic;

namespace Forms.Api
{
    public sealed record FormInstanceContext
    {
        public FormInstanceContext(
            Dictionary<string, string> keyValuePairs)
        {
            ArgumentNullException.ThrowIfNull(keyValuePairs);
            KeyValuePairs = keyValuePairs;
        }

        public Dictionary<string, string> KeyValuePairs { get; }
    }
}
