﻿using System;

namespace Forms.Api
{
    public sealed record SystemAction
    {
        private SystemAction(string actionName)
        {
            ArgumentException.ThrowIfNullOrEmpty(actionName);
            Name = actionName;
        }

        public static SystemAction ListSubjectForms { get; } = new SystemAction("ListSubjectForms");

        public static SystemAction RunExtractsAndReports { get; } = new SystemAction("RunExtractsAndReports");


        public string Name { get; }

        public override string ToString()
        {
            return Name;
        }
	}
}
