﻿using System;
using System.Threading.Tasks;

namespace Forms.Api
{
    public interface IFormInstanceContextProvider
    {
        Task<FormInstanceContext> GetFormInstanceContext(Guid instanceGuid, string tenantKey);
    }
}
