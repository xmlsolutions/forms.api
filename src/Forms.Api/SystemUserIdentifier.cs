﻿using System;
using System.ComponentModel;

namespace Forms.Api
{
    public sealed record SystemUserIdentifier
    {
        public string Value { get; }
        public SystemUserIdentifierType Type { get; }

        public SystemUserIdentifier(
            string value,
            SystemUserIdentifierType type)
        {
            ArgumentException.ThrowIfNullOrEmpty(value);

            if (!Enum.IsDefined(typeof(SystemUserIdentifierType), type))
                throw new InvalidEnumArgumentException(nameof(type), (int)type, typeof(SystemUserIdentifierType));

            Value = value;
            Type = type;
        }
    }

    public enum SystemUserIdentifierType
    {
        User = 1,
        Subject = 2,
        System = 3
    }
}
