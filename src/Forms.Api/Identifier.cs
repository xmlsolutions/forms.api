﻿
namespace Forms.Api
{
    public sealed record Identifier
    {
        public Identifier(
            string label,
            string value)
        {
            Label = label;
            Value = value;
        }

        public string Label { get; set; }

        public string Value { get; }
    }
}
