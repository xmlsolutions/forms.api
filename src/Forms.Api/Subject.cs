﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace Forms.Api
{
	public sealed record Subject
	{
        /// <summary>
        /// Information regarding a subject
        /// </summary>
        /// <param name="subjectIdentifier">The unique identifier for the subject</param>
        /// <param name="title">The subjects title</param>
        /// <param name="firstName">The subjects forename</param>
        /// <param name="surname">The subjects surname</param>
        /// <param name="dateOfBirth">The subjects date of birth</param>
        /// <param name="dateOfDeath">The subjects date of death, optional.</param>
        /// <param name="gender">The subjects gender</param>
        /// <param name="address">The subjects address</param>
        /// <param name="keyValuePairs">Other values that can be accessed via the form.</param>
        /// <param name="primaryIdentifier">The subjects primary identifier</param>
        /// <param name="secondaryIdentifier">The subjects secondary identifier</param>
        public Subject(
            string subjectIdentifier,
            string title,
            string firstName,
            string surname,
			DateTime? dateOfBirth,
			DateTime? dateOfDeath,
            string gender,
			Address address,
			Dictionary<string, string> keyValuePairs,
            Identifier primaryIdentifier,
            Identifier? secondaryIdentifier)
		{
            ArgumentNullException.ThrowIfNull(primaryIdentifier);

            SubjectIdentifier = subjectIdentifier;
			Title = title;
			FirstName = firstName;
			Surname = surname;
			DateOfBirth = dateOfBirth;
			DateOfDeath = dateOfDeath;
			Gender = gender;
			Address = address;
			KeyValuePairs = keyValuePairs;
           
            PrimaryIdentifier = primaryIdentifier;
            SecondaryIdentifier = secondaryIdentifier;
        }

		public string SubjectIdentifier { get; }
		public string Title { get; }
		public string FirstName { get; }
		public string Surname { get; }
		public DateTime? DateOfBirth { get; }
		public DateTime? DateOfDeath { get; }
		public string Gender { get; }
		public Address Address { get; }
		public Dictionary<string, string> KeyValuePairs { get; }
        public Identifier PrimaryIdentifier { get; set; }
        public Identifier? SecondaryIdentifier { get; set; }
	}
}