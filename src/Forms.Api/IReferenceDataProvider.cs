﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forms.Api
{
	public interface IReferenceDataProvider
    {
        /// <summary>
        ///    Returns reference data from an external provider
        /// </summary>
        /// <param name="referenceDataName">
        ///     Name of the reference data to return e.g. gender, religion, ethnicity
        /// </param>
        /// <param name="version">
        ///     Which version of the data to return
        /// </param>
        /// <param name="filterGroups">
        ///     If the reference data item belongs to one or more groups we can 
        ///     filter the returned groups by providing a list of required names an
        ///     example could be wards in a location
        /// </param>
        /// <param name="filterText">
        ///     Filter items by providing text to search for
        /// </param>
        /// <param name="tenantKey">The calling tenant</param>
        /// <returns></returns>
        Task<IReadOnlyCollection<ReferenceDataItem>> GetReferenceData(
			string referenceDataName, 
			int version,
            string[] filterGroups,
			string filterText,
            string tenantKey);
	}
}
