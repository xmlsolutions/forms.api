﻿using System;
using System.Collections.Generic;

namespace Forms.Api
{
    public sealed record User
    {
        public User(
            string identifier,
            string title,
            string givenNames,
            string familyName,
            string jobTitle,
            Guid organisationGuid,
            string organisationSpeciality,
            string contactId,
            string contactMainLocationName,
            string contactTypeCodeLabel,
            string contactTypeCodeName,
            string contactDetails,
            Dictionary<string, string> keyValuePairs)
        {
            Identifier = identifier;
            Title = title;
            GivenNames = givenNames;
            FamilyName = familyName;
            JobTitle = jobTitle;
            OrganisationGuid = organisationGuid;
            OrganisationSpeciality = organisationSpeciality;
            ContactId = contactId;
            ContactMainLocationName = contactMainLocationName;
            ContactTypeCodeLabel = contactTypeCodeLabel;
            ContactTypeCodeName = contactTypeCodeName;
            ContactDetails = contactDetails;
            KeyValuePairs = keyValuePairs;
        }

        public string Identifier { get; }

        public string Title { get; }

        public string GivenNames { get; }

        public string FamilyName { get; }

        public string JobTitle { get; }

        public Guid OrganisationGuid { get; }

        public string OrganisationSpeciality { get; }

        public string ContactId { get; }

        public string ContactMainLocationName { get; }

        public string ContactTypeCodeLabel { get; }

        public string ContactTypeCodeName { get; }

        public string ContactDetails { get; }

        public Dictionary<string, string> KeyValuePairs { get; }
    }
}