﻿using System;

namespace Forms.Api
{
	public sealed record FormAction
	{
        private FormAction(string actionName)
		{
			ArgumentException.ThrowIfNullOrEmpty(actionName);
            Name = actionName;
		}

        public static FormAction Create { get; } = new FormAction("Create");

		public static FormAction View { get; } = new FormAction("View");

		public static FormAction Edit { get; } = new FormAction("Edit");

        public static FormAction Withdraw { get; } = new FormAction("Withdraw");

        public static FormAction Reinstate { get; } = new FormAction("Reinstate");

		public string Name { get; }

        public override string ToString()
		{
            return Name;
		}

	}
}
