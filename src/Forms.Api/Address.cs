﻿using System;

namespace Forms.Api
{
	public sealed record Address
	{
		public Address(
            string line1,
            string line2,
            string line3,
            string line4,
            string postcode)
		{
			Line1 = line1;
			Line2 = line2;
			Line3 = line3;
			Line4 = line4;
			Postcode = postcode;
		}

		public string Line1 { get; }

		public string Line2 { get; }

		public string Line3 { get; }

		public string Line4 { get; }

		public string Postcode { get; }
	}
}