﻿using Forms.Api;
using System;
using System.Threading.Tasks;
using FormAbstractions;

namespace ExamplePlugin
{
    /// <summary>
    ///     forms4health has flexibility to plugin into most authentication contexts. Once authenticated
    ///     forms4health will delegate permissions to this plugin that determines whether the logged in 
    ///     user has the necessary privileges to edit or read the information surfaced by the form, and 
    ///     indeed whether the user should have access to the subject’s record. 
    /// </summary>
    public class AuthorisationProvider : IAuthorisationProvider
    {
        public Task<bool> UserHasAuthority(Guid? instanceGuid,
            SystemUserIdentifier systemUserIdentifier,
            string subjectIdentifier,
            string formKey,
            int formVersion,
            FormAction formAction,
            UserIdentifier instanceCreator,
            string tenantKey,
            UserIdentifier? onBehalfOf)
        {            
            //You should call your API here.
            return Task.FromResult(true);
        }

        public Task<bool> UserHasAuthority(SystemUserIdentifier systemUserIdentifier, SystemAction action, string tenantKey)
        {
            return Task.FromResult(true);
        }
    }
}
