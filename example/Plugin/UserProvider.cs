﻿using Forms.Api;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamplePlugin
{
    /// <summary>
    ///     This plugin serves to provide context about the logged in user to 
    ///     the form - for instance to default fields on the form such as 
    ///     responsible clinician — with the logged in user’s details.
    /// </summary>
    public class UserProvider : IUserProvider
    {
        public Task<User> GetUserDetail(SystemUserIdentifier systemUserIdentifier, string tenantKey)
        {
            return Task.FromResult(
                new User(
                    contactId: "20004135",
                    contactMainLocationName: "St James's Institute of Oncology",
                    contactTypeCodeLabel: "Consultant",
                    contactTypeCodeName: "CON",
                    organisationGuid: new Guid("a63efc8d-0ebe-dd11-a2ab-001e68862219"),
                    organisationSpeciality: "Clinical Oncology",
                    title: "Professor",
                    identifier: "jonesj",
                    givenNames: "James",
                    familyName: "Jones",
                    jobTitle: "Consultant Surgeon",
                    contactDetails: "01479 654321",
                    keyValuePairs: ExtraData));
        }

        public static Dictionary<string, string> ExtraData =>
            new Dictionary<string, string>
            {
                { "Speciality", "Pediatrics" },
                { "Current Ward", "St. James" },
                { "Organisation", "Inst. of Pediatric Surgeons" },
            };
    }
}
