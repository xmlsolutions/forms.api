﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Forms.Api;

namespace ExamplePlugin
{
    /// <summary>
    ///     Used to pull in the subject demographics from the surrounding context to both populate 
    ///     the CUI based subject banner as well as providing data context to the form for instance
    ///     to only present questions pertinent to adults if the subject’s age is over 18.
    /// </summary>
    public class SubjectProvider : ISubjectProvider
    {
        public Task<Subject> GetSubjectDetail(Guid instanceGuid, string subjectIdentifier, string? tenantKey)
        {
            //You should call your API here.
            return Task.FromResult(Subject);
        }

        private static Subject Subject =>
            new Subject(
                subjectIdentifier: new Guid("778C02DE-367E-4185-818F-494FF2F1D2D4").ToString(),
                title: "Mr",
                firstName: "John",
                surname: "Doe",
                dateOfBirth: new DateTime(1977, 02, 22),
                dateOfDeath: null,
                gender: "Male",
                address:
                new Address(
                    line1: "2 Headrow",
                    line2: string.Empty,
                    line3: "Leeds",
                    line4: string.Empty,
                    postcode: "LS1 2HT"),
                keyValuePairs: ExtraData,
                primaryIdentifier: new Identifier("NHS Number", "3333333"),
                secondaryIdentifier: new Identifier("PAS Number", "548845"));

        public static Dictionary<string, string> ExtraData =>
            new Dictionary<string, string>
            {
                { "Client", "Amce Garbage Collectors" },
                { "Claim", "213487183" },
                { "DateOfInjury", "2016-11-01T00:00:00" },
                { "PrimaryDiagnosticCategory", "ABI" },
                { "Category", "Cardiovascular/Hemodynamic" },
                { "Subcategory", "Cardiac Arrest" },
                { "Condition", "Cardiac Arrest" },
                { "Occurrence", "2" }
            };
    }
}
