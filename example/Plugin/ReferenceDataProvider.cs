﻿using Forms.Api;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamplePlugin
{
    /// <summary>
    ///     forms4health has sophisticated support for reference data including clinical coded data such as SNOMED subsets. 
    ///     Reference data can be defined within the form template, can be managed by forms4health as a ValueSet or can be 
    ///     delegated to a backend service. Unlike the other plugins you can have multiple Reference Data plugins so that 
    ///     different reference data can be sourced from different backend systems or terminology services. 
    /// </summary>
    class ReferenceDataProvider : IReferenceDataProvider
    {
        public async Task<IReadOnlyCollection<ReferenceDataItem>> GetReferenceData(
            string referenceDataName, 
            int version,             
            string[] filterGroups, 
            string filterText,
            string tenantKey)
        {
            // Call API here
            return await Task.FromResult(FakeReferenceData);
        }

        private static readonly ReferenceDataItem[] FakeReferenceData =
            new[]
            {
                new ReferenceDataItem(
                    label: "Not Known",
                    value: "1",
                    filterGroups: Enumerable.Empty<string>().ToArray()),
                new ReferenceDataItem(
                    label: "Male",
                    value: "2",
                    filterGroups: Enumerable.Empty<string>().ToArray()),
                new ReferenceDataItem(
                    label: "Female",
                    value: "3",
                    filterGroups: Enumerable.Empty<string>().ToArray()),
                new ReferenceDataItem(
                    label: "Not Specified",
                    value: "4",
                    filterGroups: Enumerable.Empty<string>().ToArray()),
            };
    }
}
