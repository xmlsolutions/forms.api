# Readme
 to revert
## Nuget repository

<https://dev.azure.com/AireLogic/Public/_artifacts/feed/AireForms/>

## Aireforms 10.0.0 breaking change to plugins

From 10.0.0 the way we load plugins into AireForms has slightly changed.
Plugin authors will need to apply 2 small changes to their plugins csproj file.

The first is adding `<EnableDynamicLoading>true</EnableDynamicLoading>` to the property group and secondly adding `<ExcludeAssets>runtime</ExcludeAssets>` to the package reference.

```xml
  <PropertyGroup>
    <TargetFramework>net8.0</TargetFramework>
    <ImplicitUsings>enable</ImplicitUsings>
    <Nullable>enable</Nullable>
    <EnableDynamicLoading>true</EnableDynamicLoading>
  </PropertyGroup>
  <ItemGroup>
    <PackageReference Include="AireLogic.AireForms.Api" Version="x.x.x">
        <ExcludeAssets>runtime</ExcludeAssets>
    </PackageReference>
  </ItemGroup>
```
