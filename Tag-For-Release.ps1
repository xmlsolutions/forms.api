param(
    [Parameter(Mandatory=$true)]    
    [string]$version)

    function CheckExitCode([string]$failureMessage = "An Error Occured") {
        if ($LastExitCode -ne 0) {
            Write-Error $failureMessage;
            exit
        }
    }

    if ($(git rev-parse --abbrev-ref HEAD) -ne "master") {
        Write-Error "Current branch is not master!";
        Read-Host -Prompt "Press Enter to exit"
        exit
    }

    & git @("pull")
    CheckExitCode

    & git @("tag", "-a", $version, "-m", $version ) 
    CheckExitCode;

    & git @("push", "origin", $version) 